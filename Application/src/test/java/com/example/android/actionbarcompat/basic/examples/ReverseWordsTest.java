package com.example.android.actionbarcompat.basic.examples;

import org.junit.BeforeClass;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;


public class ReverseWordsTest {
    private static ReverseWords r;

    @BeforeClass
    public static void setUp(){
        r = new ReverseWords();
    }

    @Test
    public void testEmptyWord() {
        assertEquals("", r.reverse(""));
    }

    @Test
    public void testSpaceWord() {
        assertEquals(" ", r.reverse(" "));
    }

    @Test
    public void testOneWord() {
        assertEquals("Test", r.reverse("Test"));
        assertEquals("A", r.reverse("A"));
    }

    @Test
    public void testString() {
        assertEquals("! Welt Hallo", r.reverse("Hallo Welt !"));
    }

    @Test
    public void testHyphenInWord() {
        assertEquals("Hans-Peter Hallo", r.reverse("Hallo Hans-Peter"));
    }

    @Test
    public void testDoubleReverseWord() {
        assertEquals("1 2 3 4", r.reverse(r.reverse("1 2 3 4")));
    }

    @Test
    public void testTrailingSpaces() {
        assertEquals("  b a   ", r.reverse("   a b  "));
    }

    @Test
    public void testTab() {
        assertEquals("1     2   3", r.reverse("3   2     1"));
    }
    @Test
    public void testTabExplicit() {
        assertEquals("1 \t\t\t 2 \t\t 3", r.reverse("3 \t\t 2 \t\t\t 1"));
    }

    @Test
    public void testLineBreak() {
        assertEquals("Welt\nHallo", r.reverse("Hallo\nWelt"));
    }

}