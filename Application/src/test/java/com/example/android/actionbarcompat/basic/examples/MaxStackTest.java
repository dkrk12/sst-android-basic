package com.example.android.actionbarcompat.basic.examples;

import static junit.framework.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MaxStackTest {

    private static final int stackSize = 3;

    private MaxStack stack;

    @Before
    public void setUp() {
        stack = new MaxStack(stackSize);
    }

    @Test
    public void testValidPushPop() {
        stack.push(1);
        assertEquals(1, stack.pop());
    }

    @Test
    public void testOrdering() {
        stack.push(1);
        stack.push(2);
        stack.push(3);
        assertEquals(3, stack.pop());
        assertEquals(2, stack.pop());
        assertEquals(1, stack.pop());
    }

    @Test(expected = RuntimeException.class)
    public void testEmptyStack() {
        stack.pop();
    }

    @Test(expected = RuntimeException.class)
    public void testFullStack() {
        fillStack();
        stack.push(4);
    }

    private void fillStack() {
        for (int i = 0; i < stackSize; i++) {
            stack.push(i);
        }
    }

}
