package com.example.android.actionbarcompat.basic.examples;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

public class BankAccountTest {
    private final static String customerName = "Daniel Kreck";
    private final static Double balance = 5.00;

    private BankAccount account;

    @Before
    public void setUp() {
        account = new BankAccount(customerName, balance);
    }

    @Test(expected = RuntimeException.class)
    public void shouldBeFrozenOnDebit() {
        account.freezeAccount();
        account.debit(1.00);
    }

    @Test(expected = RuntimeException.class)
    public void overdraftNotAllowed() {
        account.debit(balance + 0.01);
    }

    @Test(expected = IllegalArgumentException.class)
    public void amountLowerZeroNotAllowed() {
        account.debit(-0.01);
    }

    @Test
    public void testValidDebit() {
        account.debit(4.99);
        assertEquals(0.01, account.getBalance(), 0.001);

        setUp();
        account.debit(0.01);
        assertEquals(4.99, account.getBalance(), 0.001);

        setUp();
        account.debit(2.5);
        assertEquals(2.5, account.getBalance(), 0.001);

        setUp();
        account.debit(0.0);
        assertEquals(balance, account.getBalance());
    }

    @Test(expected = RuntimeException.class)
    public void shouldBeFrozenOnCredit() {
        account.freezeAccount();
        account.credit(1.00);
    }

    @Test(expected = RuntimeException.class)
    public void creditValueMustBePositive() {
        account.credit(-0.01);
    }

    @Test
    public void testValidCredit() {
        account.credit(0);
        assertEquals(balance, account.getBalance());

        setUp();
        account.credit(balance);
        assertEquals(balance * 2, account.getBalance());

        setUp();
        account.credit(Double.MAX_VALUE);
        assertEquals(balance + Double.MAX_VALUE, account.getBalance());
        assertTrue(account.getBalance() > Double.MAX_VALUE);
    }

}
